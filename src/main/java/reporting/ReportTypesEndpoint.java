package reporting;

import catalog.dto.reporting.ReportExportFormat;
import catalog.dto.reporting.ReportParameterDefinitionDetails;
import catalog.dto.reporting.WebUserDetails;
import com.red61.via.api.exceptions.RestApiException;
import org.apache.log4j.Logger;
import reporting.engine.ReportTitleAndDescriptionDetails;
import com.red61.via.api.auth.WebAuthenticatedUser;
import reporting.rest.dto.ReportDTO;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

import com.red61.via.api.auth.WebUserAuthRequired;


@Path("/report-types")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@WebUserAuthRequired
public class ReportTypesEndpoint {

	@Inject
	@WebAuthenticatedUser
	private WebUserDetails webUserDetails;
	
	@Inject private ExportEngine exportEngine;


	private final Logger logger = Logger.getLogger(this.getClass().getName());

	@POST
	@Path("/{id}/run")
	@Produces({MediaType.APPLICATION_JSON, "application/pdf", MediaType.TEXT_HTML, "application/rtf", MediaType.TEXT_PLAIN, "application/vnd.oasis.opendocument.text", "application/vnd.ms-excel", "text/csv", MediaType.TEXT_HTML})
	public Response runReport(@PathParam("id") Integer reportTypeId, @DefaultValue("csv") @QueryParam("dataType") ReportExportFormat dataType, ReportDTO reportDTO) {

		if(dataType == null){
			throw new RestApiException("Data type is required", Response.Status.BAD_REQUEST);
		}

		ReportTitleAndDescriptionDetails reportTypeForUser = reportingEngine.findBasicReportTypeForUser(reportTypeId, webUserDetails);
		logger.debug("runReport hit for report type [" + reportTypeId + "], '" + reportTypeForUser.getTitle() + "', data type [" + dataType + "]"); //NOI18N

		Map<String, ReportParameterDefinitionDetails> paramDefs = reportingEngine.mapParamDTOs(reportTypeId, 0,
				reportDTO.getParams(), false);

		Map<String, ReportParameterDefinitionDetails> fullParamDefs = reportingEngine.populateEmptyParamDefsFromParameters(reportTypeId,
				0, paramDefs);

		if (fullParamDefs == null) {
			throw new RestApiException("Failed to load parameters", Response.Status.INTERNAL_SERVER_ERROR);
		}

		String typeName = reportTypeForUser.getTitle() == null ? "Not Available" : reportTypeForUser.getTitle();

		try {
			typeName = URLEncoder.encode(typeName, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.warn("Filename encoding failed: " + e.getMessage(), e); //NOI18N
			typeName = "Custom-Report";
		}

		OutputStream outputStream = exportEngine.exportReportToOutputStream(reportDTO.getReportTypeId(), fullParamDefs, dataType, typeName, webUserDetails);

		return Response.ok(outputStream).header("Content-Type", dataType.getMediaType()).header("Content-Disposition",
				"attachment; filename=\"" + typeName + "." + dataType.getFileExtension() + "\"").build();

	}


}
