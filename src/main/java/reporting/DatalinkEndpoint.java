/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reporting;

import catalog.dto.reporting.ReportExportFormat;
import catalog.dto.reporting.ReportParameterDefinitionDetails;
import catalog.dto.reporting.WebUserDetails;
import catalog.ejb.local.interfaces.AuthenticationControllerLocal;
import com.red61.via.api.auth.WebAuthenticatedUser;
import com.red61.via.api.exceptions.RestApiException;
import io.swagger.annotations.ApiOperation;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.Base64;
import java.util.Map;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.security.auth.message.AuthException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.log4j.Logger;
import reporting.engine.ReportingEngine;

/**
 *
 * @author will
 */
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Path("/datalinks")
public class DatalinkEndpoint {

	@Inject private ExportEngine exportEngine;
	
	@EJB
	private AuthenticationControllerLocal authController;

	@Inject
	@WebAuthenticatedUser
	private WebUserDetails webUserDetails;

	private final Logger logger = Logger.getLogger(this.getClass().getName());
	
	@ApiOperation(value = "Export the data of the saved report definition - Requires HTTP Basic Auth")
	@GET
	@Path("/{savedReportId}")
	@Produces({MediaType.APPLICATION_JSON, "application/pdf", MediaType.TEXT_HTML, "application/rtf", MediaType.TEXT_PLAIN, "application/vnd.oasis.opendocument.text", "application/vnd.ms-excel", "text/csv", MediaType.TEXT_HTML})
	public Response export(@Context HttpServletRequest request, @Context HttpServletResponse response,
			@PathParam("savedReportId") Integer savedReportId, @QueryParam("dataType") ReportExportFormat reportFormat, @QueryParam("token") String token) {

		logger.debug("export called for " + savedReportId);

		logger.debug("request [" + request.getQueryString() + "]");

		// Do we allow that user?
		LoginCredentials loginCredentials;
		try {
			loginCredentials = getLoginCredentials(request);
		}
		catch (AuthException ex) {
			logger.warn("doGet(): " + ex.getMessage());//NOI18N
			return buildAuthresponse(ex.getMessage());
		}
		logger.info("export called for " + savedReportId + " by " + loginCredentials.username);

		int reportDefId = savedReportId;
		/*
		 * Grab token from request
		 * Get web user details from token
		 * Get report definition from token
		 */
		WebUserDetails wud;
		try {
			if(token != null && !token.isEmpty()) {
				logger.debug("Rquest with token");
				wud = re.getWebUserDetailsFromReportAPIToken(reportDefId, loginCredentials.getUsername(), loginCredentials.getPassword(), token);
			}
			else {
				logger.debug("Request as reporting user");
				wud = authController.checkReportingLogin(loginCredentials.getUsername(), loginCredentials.getPassword());
			}
			logger.info("export called for " + savedReportId + " by " + wud);
		}
		catch (Exception ex) {
			logger.warn(ex);
			return buildAuthresponse(ex.getMessage());
		}

		if (wud == null) {
			return buildError("Failed to find webUserDetails");
		}

		if (reportDefId == -1) {
			return buildError("No userReportId supplied");
		}

		try {
			// Check that the user still has access to this report, before continuing
			re.validateSavedReportIdForUser(reportDefId, wud, false);
		}
		catch (RestApiException e) {
			logger.error("Report access check failed: " + e.getMessage(), e); //NOI18N
			return buildError(e.getMessage());
		}
		if (reportFormat == null) {
			logger.debug("No report format given, default to html export"); //NOI18N
			reportFormat = ReportExportFormat.HTMLEXPORT;
		}

		logger.debug("Report Def ID: " + reportDefId + " format: " + reportFormat);
		int reportTypeId = re.getReportIdForReportDefId(reportDefId);

		Map<String, ReportParameterDefinitionDetails> paramDefs = re.getParamDefsForReportDefId(reportDefId);
		paramDefs = re.populateEmptyParamDefsFromParameters(reportTypeId, reportDefId, paramDefs);
		if (paramDefs == null) {
			return buildError("paramDefs null");
		}

		for (Object p : request.getParameterMap().keySet()) {
			String param = (String) p;
			if (!param.toUpperCase().equals(param)) {
				continue;
			}
			if (paramDefs.containsKey(param)) {
				logger.info("Parameter [" + param + "] overide");
				ReportParameterDefinitionDetails paramDef = paramDefs.get(param);
				paramDef.setValue(request.getParameter(param));
				paramDefs.put(param, paramDef);
			} else {
				logger.warn("Parameter [" + param + "] overide invalid");
			}
		}

		String reportTypeName = re.getReportNameforReportTypeId(reportTypeId);
		String reportDefinitionName = re.getReportDefinitionNameforReportDefinitionId(reportDefId);
		if (reportTypeName == null) {
			reportTypeName = "Not available";
		}
		if (reportDefinitionName == null) {
			reportDefinitionName = "Not Available";
		}
		String name = "Custom-" + reportTypeName + "-" + reportDefinitionName;

		OutputStream outputStream = re.exportReportToOutputStream(reportTypeId, paramDefs, reportFormat, name, wud);

		Response.ResponseBuilder builder = Response.ok(outputStream).header("Content-Type", reportFormat.getMediaType());
		if (!reportFormat.equals(ReportExportFormat.HTMLEXPORT) && !reportFormat.equals(ReportExportFormat.HTML)) {
			logger.debug("exportReport(): reportFormat IS [" + reportFormat + "]");//NOI18N
			builder.header("Content-Disposition", "attachment;filename=" + URLEncoder.encode(name) + "." + reportFormat.getFileExtension());
		}
		return builder.build();
	}

	private Response buildError(String error) {
		logger.warn(error);
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(error).build();
	}

	private LoginCredentials getLoginCredentials(HttpServletRequest request) throws AuthException {

		// Get Authorization header
		String auth = request.getHeader("Authorization");
		if (auth == null) {
			logger.warn("No Auth");
			throw new AuthException("No Auth");
		}
		if (!auth.toUpperCase().startsWith("BASIC ")) {
			logger.warn("Only Accept Basic Auth");
			throw new AuthException("Incorrect Auth Type");
		}

		// Get encoded user and password, comes after "BASIC "
		String userpassEncoded = auth.substring(6);

		String userpassDecoded = new String(Base64.getDecoder().decode(userpassEncoded));

		String account[] = userpassDecoded.split(":");
		if(account.length != 2) {
			throw new AuthException("Username and password must be supplied");
		}
		return new LoginCredentials(account[0], account[1]);
	}

	private Response buildAuthresponse(String message) {
		// Not allowed, so report he's unauthorized
		return Response.status(Response.Status.UNAUTHORIZED)
				.header("WWW-Authenticate", "Basic realm=\"Data Link\"")
				.entity(message)
				.type(MediaType.TEXT_PLAIN_TYPE)
				.header("Pragma", "no-cache")
				.header("Cache-Control", "no-cache, no-store")
				.header("Connection", "Close")
//				.header("Expires", "Mon, 26 Jul 1997 05:00:00 GMT")
//				.header("Server", "Red61")
				.build();
	}
	
	private class LoginCredentials {

		private final String username;
		private final String password;
				
		private LoginCredentials(String username, String password) {
			this.username = username;
			this.password = password;
		}

		public String getUsername() {
			return username;
		}

		public String getPassword() {
			return password;
		}

	}

}
