package reporting;

import au.com.bytecode.opencsv.CSVWriter;
import catalog.dto.reporting.ReportExportFormat;
import catalog.dto.reporting.ReportParameterDefinitionDetails;
import catalog.dto.reporting.ReportParameterDetails;
import catalog.dto.reporting.WebUserDetails;
import catalog.util.DatabaseConnection;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import static com.google.common.collect.Lists.newArrayList;
import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.sql.Connection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.ejb.FinderException;
import javax.ws.rs.core.Response;
import net.sf.jasperreports.engine.JRBand;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRSection;
import net.sf.jasperreports.engine.JRVariable;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignBand;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.design.JRDesignGroup;
import net.sf.jasperreports.engine.design.JRDesignStaticText;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRTextExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;
import net.sf.jasperreports.engine.type.WhenNoDataTypeEnum;
import net.sf.jasperreports.engine.xml.JRXmlDigester;
import net.sf.jasperreports.engine.xml.JRXmlDigesterFactory;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleHtmlExporterOutput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleTextReportConfiguration;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;
import org.apache.log4j.Logger;
import org.xml.sax.InputSource;

import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Maps.newLinkedHashMap;
import static com.google.common.collect.Sets.newHashSet;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author will
 */
public class ExportEngine {

	private final Logger logger = Logger.getLogger(this.getClass().getName());

	//============================REPORT CREATION ==================================================================
	/*To create a report we need the parameters, the format.
     *returns - a byte[]
     *        - null if there was a problem creating the report
     *
	 */
	private byte[] createPDFReport(int reportTypeId, Map<String, ReportParameterDefinitionDetails> paramDefs, WebUserDetails webUserDetails) throws FinderException {

		JasperPrint jp = makeJasperPrint(reportTypeId, paramDefs, webUserDetails);
		if (jp != null) {
			try {
				return JasperExportManager.exportReportToPdf(jp);
			}
			catch (JRException ex) {
				logger.error("There has been an error exporting the report to PDF format: ", ex);
				return null;
			}
		}
		return null;
	}

	/*   Make a jasperprint object by giving it the appropriate params
    returns - a jasper print object if no problems encountered
    - null if there was a problem
	 */
	private JasperPrint makeJasperPrint(int reportTypeId, Map<String, ReportParameterDefinitionDetails> paramDefs, WebUserDetails webUserDetails) throws FinderException {

		if (paramDefs.isEmpty()) {
			logger.debug("ParamDefs is empty - defining a report");
		}

		String webUserInfo = ((webUserDetails.getUserCode() == null || webUserDetails.getUserCode().isEmpty()) ? webUserDetails.toString() : webUserDetails.getUserCode());

		logger.info("makeJasperPrint(): CREATING REPORT Type [" + reportTypeId + "] with paramsDefs [" + paramDefs + "] for " + webUserInfo);
		//reporting stuff =====================================
		JasperPrint jp;
		Map<String, Object> parameters = getProcessedParameters(reportTypeId, paramDefs, webUserDetails);

		String xmlContent = getXmlContentFromDB(reportTypeId);
		if (xmlContent == null || xmlContent.isEmpty()) {
			return null;
		}

		LinkedHashMap params = getParameters(reportTypeId);
		if (params == null) {
			return null;
		}
		/*Set up params hashmap*/

		List<String> groupsToRemove = newArrayList();

		Set paramDefCodeSet = paramDefs.keySet();
		Iterator it = paramDefCodeSet.iterator();
		boolean summary = false;
		while (it.hasNext()) {
			String paramDefCode = (String) it.next();

			ReportParameterDefinitionDetails pDef = paramDefs.get(paramDefCode);

			/*
            use paramDef id to index the params linkedhashmap
			 */
			ReportParameterDetails p = (ReportParameterDetails) params.get(paramDefCode);
			String code = pDef.getCode();
			logger.debug("param code = " + code);

			if (p.getCode().equals(SUMMARY_PARAMETER_CODE)) {
				summary = !pDef.getValue().isEmpty();
			}

			if (p.getGroupOption() == 1 && pDef.getValue().isEmpty()) {
				//Group option which has not been selected
				groupsToRemove.add(p.getName());
			}

			if (pDef.getHiddenOption() == 1) {
				//need to exclude this field

				logger.info("makeJasperPrint(): for report type [" + reportTypeId + "] field [" + pDef + "] has hidden == 1, so must exclude it");
				if (p.getWhereOption() == 0) {
					xmlContent = removeFieldFromReport(code, xmlContent);
					logger.debug("Attempting to remove field with name: " + code);
					// jasperDesign.removeField(code);
					if (xmlContent.isEmpty()) {
						return null;
					}
				} else {
					xmlContent = removeGroupFromReport(newArrayList(p), reportTypeId, xmlContent);
					logger.debug("Attempting to remove group with name: " + code + "GROUP");
					//jasperDesign.removeGroup(code+"GROUP");
					// jasperDesign.removeGroup("orderGroup");
					if (xmlContent.isEmpty()) {
						return null;
					}

				}
			}
		}

		/*Now must set the locale of the report*/
		Locale locale = Locale.UK;
		parameters.put(JRParameter.REPORT_LOCALE, locale);
		parameters.put("LOCALSERVERID", GetEJBPrefix.getLocalServerId());

		//set up the connection to the database
		Connection con = DatabaseConnection.getReportsConnection();

		//now send the report
		JasperDesign jasperDesign;
		JRXmlDigester d;

		try {

			d = JRXmlDigesterFactory.createDigester();
			JRXmlLoader loader = new JRXmlLoader(d);
			logger.info("makeJasperPrint(): Loading Report Type [" + reportTypeId + "] for " + webUserInfo);
			jasperDesign = loader.loadXML(new InputSource(new StringReader(xmlContent)));
			if (jasperDesign.getNoData() == null) {
				jasperDesign.setNoData(getNoDataBand());
			}
			jasperDesign.setWhenNoDataType(WhenNoDataTypeEnum.NO_DATA_SECTION);
			//If some groups have not been chosen then remove them
			List<JRVariable> varsToRemove = newArrayList();
			for (String groupName : groupsToRemove) {
				JRDesignGroup group = (JRDesignGroup) jasperDesign.getGroupsMap().get(groupName);
				logger.debug("Retrieved group [" + groupName + "]");
				for (Object obj : jasperDesign.getVariablesList()) {
					JRVariable var = (JRVariable) obj;
					if (var.getResetGroup() == group) {
						logger.debug("This var uses removed group ; Removing");
						varsToRemove.add(var);
					}
				}
				jasperDesign.removeGroup(group);
				logger.info("Removed group [" + groupName + "]");

			}
			logger.debug("Removing all used variables");
			for (JRVariable var : varsToRemove) {
				jasperDesign.removeVariable(var);
			}
			if (summary) {
				/*
				 * The "summary" option has been checked so we want to hide the detail section.
				 * Do this by setting a printwhenexpression which will always be false.
				 */
				logger.info("makeJasperPrint(): Report type [" + reportTypeId + "]  for [" + webUserInfo + "] is is a summary report; removing detail band");
				JRSection detailSection = jasperDesign.getDetailSection();
				JRDesignExpression expr = new JRDesignExpression();
				expr.setText("Boolean.FALSE");
				for (JRBand bandInDetails : detailSection.getBands()) {
					JRDesignBand detailBand = (JRDesignBand) bandInDetails;
					detailBand.setPrintWhenExpression(expr);
				}
			}

			logger.debug("makeJasperPrint(): Compile Report type [" + reportTypeId + "] XML byte length is [" + xmlContent.length() + "] for " + webUserInfo + "");
			JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
			logger.info("makeJasperPrint(): Filling Report type [" + reportTypeId + "] for " + webUserInfo + " with parameters [" + parameters + "]");

			jp = JasperFillManager.fillReport(jasperReport, parameters, con);

			if (jp != null) {
				logger.info("makeJasperPrint(): For report type [" + reportTypeId + "] with paramsDefs [" + paramDefs + "] for " + webUserInfo + " Created a " + jp.getPages().size() + " page report");
				return jp;
			} else {
				logger.warn("makeJasperPrint(): ERROR : Japser print object not created correctly For report type [" + reportTypeId + "] with paramsDefs [" + paramDefs + "] for " + webUserInfo);
				return null;
			}

		}
		catch (JRException ex) {
			logger.error("There has been a JRException whilst creating the jasperPrint: ", ex);
			return null;
		} //catch (ParserConfigurationException ex) {
		//   logger.error("There has been a Parser Configuration Exception whilst creating the jasperPrint: ", ex);
		//  return null;
		//}catch (SAXException ex) {
		//   logger.error("There has been a SAX Exception whilst creating the jasperPrint: ", ex);
		//  return null;
		//    }
		catch (Exception ex) {
			logger.error("There has been an exception whilst creating the jasperPrint", ex);
			return null;
		}
		finally {
			try {
				//close db connection
				con.close();
			}
			catch (Exception ex) {
				logger.error("There has been an exception closing the connection to the database: ", ex);
			}
		}
	}

	private JRDesignBand getNoDataBand() {
		JRDesignBand noData = new JRDesignBand();
		JRDesignStaticText staticText = new JRDesignStaticText();
		staticText.setText("Report parameters returned no matching data.");
		staticText.setHeight(10);
		staticText.setWidth(400);
		staticText.setX(0);
		staticText.setY(0);
		staticText.setFontName("DejaVu Sans Red61");
		staticText.setFontSize(8f);
		staticText.setBold(true);
		staticText.setPdfEmbedded(true);
		staticText.setBackcolor(Color.white);
		staticText.setForecolor(Color.black);
		noData.setHeight(10);
		noData.addElement(staticText);
		return noData;
	}

	public ByteArrayOutputStream exportReportToOutputStream(int reportTypeId, Map<String, ReportParameterDefinitionDetails> paramDefs, ReportExportFormat reportFormat,
			String name, WebUserDetails webUserDetails) {
		logger.info("exportReportToOutputStream(): CALLED WITH (reportTypeId [" + reportTypeId + "], paramDefs [" + paramDefs + "], reportFormat [" + reportFormat + "], name [" + name + "], webUserDetails [" + webUserDetails + "])");//NOI18N

		ByteArrayOutputStream out = new ByteArrayOutputStream();

		try {
			logger.debug("Creating report as " + name + " in " + reportFormat);
			if (reportFormat.equals(ReportExportFormat.PDF)) {
				long start = System.currentTimeMillis();
				byte[] pdf = createPDFReport(reportTypeId, paramDefs, webUserDetails);
				long timeTaken = System.currentTimeMillis() - start;
				logger.info("Report type [" + name + "] requested by [" + webUserDetails + "] as [" + reportFormat + "] took [" + timeTaken + "]ms to execute");//NOI18N
				if (pdf != null) {
					out.write(pdf);
				} else {
					throw new RestApiException("Error creating report", Response.Status.INTERNAL_SERVER_ERROR);
				}
			} //if its an csv report
			else if (reportFormat.equals(ReportExportFormat.CSV)) {
				Map<String, Object> parameters = getProcessedParameters(reportTypeId, paramDefs, webUserDetails);
				long start = System.currentTimeMillis();
				String csv = getRawCSVExport(reportTypeId, paramDefs, parameters);
				//Write csv output to output stream
				logger.debug("exportRawCSV: Writing csv file to output");
				out.write(csv.getBytes());
				long timeTaken = System.currentTimeMillis() - start;
				logger.info("Report type [" + name + "] requested by [" + webUserDetails + "] as [" + reportFormat + "] took [" + timeTaken + "]ms to execute");//NOI18N
			} else if (reportFormat.equals(ReportExportFormat.HTMLEXPORT)) {
				Map<String, Object> parameters = getProcessedParameters(reportTypeId, paramDefs, webUserDetails);
				long start = System.currentTimeMillis();
				String html = getImportableHTMLExport(reportTypeId, paramDefs, parameters);
				//Write html output to output stream
				logger.debug("exportImportableHTML: Writing html to output");
				out.write(html.getBytes());
				long timeTaken = System.currentTimeMillis() - start;
				logger.info("Report type [" + name + "] requested by [" + webUserDetails + "] as [" + reportFormat + "] took [" + timeTaken + "]ms to execute");//NOI18N
			} else {
				JasperPrint jasperPrint = makeJasperPrint(reportTypeId, paramDefs, webUserDetails);
				if (reportFormat.equals(ReportExportFormat.HTML)) {
					HtmlExporter exporter = new HtmlExporter();
					exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
					exporter.setExporterOutput(new SimpleHtmlExporterOutput(out));
					try {
						long start = System.currentTimeMillis();
						exporter.exportReport();
						long timeTaken = System.currentTimeMillis() - start;
						logger.info("Report type [" + name + "] requested by [" + webUserDetails + "] as [" + reportFormat + "] took [" + timeTaken + "]ms to execute");//NOI18N
					}
					catch (JRException ex) {
						logger.error("There has been an error exporting the report to HTML format: ", ex);
						throw new RestApiException("Error creating report", Response.Status.INTERNAL_SERVER_ERROR);
					}
				} else if (reportFormat.equals(ReportExportFormat.RTF)) {
					JRRtfExporter exporter = new JRRtfExporter();
					exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
					exporter.setExporterOutput(new SimpleWriterExporterOutput(out));
					try {
						long start = System.currentTimeMillis();
						exporter.exportReport();
						long timeTaken = System.currentTimeMillis() - start;
						logger.info("Report type [" + name + "] requested by [" + webUserDetails + "] as [" + reportFormat + "] took [" + timeTaken + "]ms to execute");//NOI18N
					}
					catch (JRException ex) {
						logger.error("There has been an error exporting the report to Rich Text format: ", ex);
						throw new RestApiException("Error creating report", Response.Status.INTERNAL_SERVER_ERROR);
					}

				} else if (reportFormat.equals(ReportExportFormat.TXT)) {
					JRTextExporter exporter = new JRTextExporter();
					exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
					exporter.setExporterOutput(new SimpleWriterExporterOutput(out));

					SimpleTextReportConfiguration conf = new SimpleTextReportConfiguration();
					conf.setCharWidth((float) 5.0);
					conf.setCharHeight((float) 10.0);
					exporter.setConfiguration(conf);

					try {
						long start = System.currentTimeMillis();
						exporter.exportReport();
						long timeTaken = System.currentTimeMillis() - start;
						logger.info("Report type [" + name + "] requested by [" + webUserDetails + "] as [" + reportFormat + "] took [" + timeTaken + "]ms to execute");//NOI18N
					}
					catch (JRException ex) {
						logger.error("There has been an error exporting the report to text format: ", ex);
						throw new RestApiException("Error creating report", Response.Status.INTERNAL_SERVER_ERROR);
					}
				} else if (reportFormat.equals(ReportExportFormat.ODT)) {
					JROdtExporter exporter = new JROdtExporter();
					exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
					exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(out));
					long start = System.currentTimeMillis();
					exporter.exportReport();
					long timeTaken = System.currentTimeMillis() - start;
					logger.info("Report type [" + name + "] requested by [" + webUserDetails + "] as [" + reportFormat + "] took [" + timeTaken + "]ms to execute");//NOI18N
				} else if (reportFormat.equals(ReportExportFormat.XLS)) {
					JRXlsExporter exporter = new JRXlsExporter();
					exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
					exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(out));
					long start = System.currentTimeMillis();
					exporter.exportReport();
					long timeTaken = System.currentTimeMillis() - start;
					logger.info("Report type [" + name + "] requested by [" + webUserDetails + "] as [" + reportFormat + "] took [" + timeTaken + "]ms to execute");//NOI18N
				} else {
					throw new RestApiException("Invalid output type: " + reportFormat, Response.Status.BAD_REQUEST);
				}
			}
		}
		catch (RestApiException re) {
			throw re;
		}
		catch (Exception e) {
			logger.error("Unable to export report for " + webUserDetails.getUserCode(), e);
			throw new RestApiException("Unable to export report for " + webUserDetails.getUserCode(), Response.Status.INTERNAL_SERVER_ERROR);
		}

		return out;
	}

	/*============================== XML MANIPULATION===========================================================*/
 /*retreives the xml_content from the database using a report's id
    Returns    -the xml string if finds correct record.
    -"" if no matching record was found or if there was an error
     *          - null if there was an error
	 */
	private String getXmlContentFromDB(int reportTypeId) {

		Connection conn = null;
		PreparedStatement s = null;
		ResultSet results = null;
		String xmlContent = "";

		try {
			conn = DatabaseConnection.getReportsConnection();
			s = conn.prepareStatement("SELECT * FROM report_type WHERE id=?");
			s.setInt(1, reportTypeId);
			results = s.executeQuery();
			while (results.next()) {
				xmlContent = results.getString("xml_content");
			}
			/*will be "" if nothing was found, a big string of xml if it was.*/
			return xmlContent;

		}
		catch (SQLException ex) {
			logger.warn("There has been an error retreiving the XML content from the database: ", ex);
			return null;
		}
		catch (Exception ex) {
			logger.warn("There has been an error retreiving the XML content from the database: ", ex);
			return null;
		}
		finally {
			try {
				if (results != null) {
					results.close();
				}
				if (s != null) {
					s.close();
				}
				if (conn != null) {
					conn.close();
				}
			}
			catch (Exception ex) {
				logger.warn("There has been a problem closing the connection to database: ", ex);
			}
		}
	}

	/*if where option == 1 for the parameter and hidden == 1, then we must remove a group footer and header
     *from the report. Recursive call to also remove any children parameters from report also. i.e if event removed, so should perf.
    Returns  - the xmlString if no errors occurred
    - null of there was an error
     *               - "" if nothing happened
	 */
	private String removeGroupFromReport(List<ReportParameterDetails> parameters, int reportTypeId, String xmlContent) {

		DocumentBuilder builder;
		Document doc;
		try {

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(true);
			factory.setNamespaceAware(true);
			builder = factory.newDocumentBuilder();
			doc = builder.parse(new InputSource(new StringReader(xmlContent)));
			NodeList nodes = doc.getElementsByTagName("group");

			for (ReportParameterDetails p : parameters) {
				logger.debug("Attempting to remove group for code:" + p.getCode());
				String targetStr = p.getCode() + "GROUP";
				for (int i = 0; i < nodes.getLength(); i++) {
					Element elem = (Element) nodes.item(i);
					logger.debug("removeGroupFromReport: Name of node: " + elem.getTagName());
					String nameStr = elem.getAttribute("name");
					logger.debug("removeGroupFromReport: value of name attr: " + nameStr);

					if (nameStr.equals(targetStr)) {
						logger.debug("In " + targetStr);
						//get the group footer
						elem.setAttribute("isStartNewPage", "false");
						NodeList groupFooterList = elem.getElementsByTagName("groupFooter");
						NodeList groupHeaderList = elem.getElementsByTagName("groupHeader");
						//deal with the group footer
						for (int j = 0; j < groupFooterList.getLength(); j++) {
							Element groupFooter = (Element) groupFooterList.item(j);
							NodeList bandList = groupFooter.getElementsByTagName("band");

							for (int k = 0; k < bandList.getLength(); k++) {
								Element band = (Element) bandList.item(k);
								band.setAttribute("height", "0");
								//      NodeList textFieldList = band.getElementsByTagName("textField");
								NodeList childrenList = band.getChildNodes();
								for (int l = 0; l < childrenList.getLength(); l++) {
									Node textNode = childrenList.item(l);
									textNode.getParentNode().removeChild(textNode);
									l--;
								}
							}
						}
						//now must deal with group header info
						for (int j = 0; j < groupHeaderList.getLength(); j++) {
							Element groupHeader = (Element) groupHeaderList.item(j);
							NodeList bandList = groupHeader.getElementsByTagName("band");

							for (int k = 0; k < bandList.getLength(); k++) {
								Element band = (Element) bandList.item(k);
								band.setAttribute("height", "0");
								NodeList childrenList = band.getChildNodes();
								for (int l = 0; l < childrenList.getLength(); l++) {
									Node textNode = childrenList.item(l);
									textNode.getParentNode().removeChild(textNode);
									l--;
								}
							}
						}
					}
				}
			}
			/*BASE CASE*/
			if (getChildren(parameters, reportTypeId).isEmpty()) { // none of the current 'children' collection have any more children
				logger.debug("Hit base case of recursion, returning xmlcontent");
				String str = convertDocToString(doc);
				//it will be null if there has been an error
				if (str != null) {
					return str;
				} else {
					return null;
				}
				/*Recursive call*/
			} else {
				logger.debug("Recursing.......");
				String str = convertDocToString(doc);
				if (str != null) {
					return removeGroupFromReport(newArrayList(getChildren(parameters, reportTypeId)), reportTypeId, str);
				} else {
					return null;
				}
			}

		}
		catch (ParserConfigurationException ex) {
			logger.error("There has been an error setting up the document builder factory: ", ex);
			return null;
		}
		catch (SAXException ex) {
			logger.error("There has been an error parsing the xml string passed as an argument: ", ex);
			return null;
		}
		catch (IOException ex) {
			logger.error("There has been an error parsing the xml string passed as an argument: ", ex);
			return null;
		}
		catch (Exception ex) {
			logger.error("There has been an error parsing the xml string passed as an argument: ", ex);
			return null;
		}
	}

	/*NB - conversion to a Document object removes DTD declaration at top of document!!!!!
    To remove a field from the report we must remove the appropriate textfield
     *Returns - string of xml if no problems occurred
     *          null otherwise
	 */
	private String removeFieldFromReport(String code, String xmlContent) {

		DocumentBuilder builder;
		Document doc;
		try {

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(true);
			factory.setNamespaceAware(true);
			builder = factory.newDocumentBuilder();

			doc = builder.parse(new InputSource(new StringReader(xmlContent)));

			logger.debug("After conversion to Document object , doctype is: " + doc.getDoctype().toString());
			String testStr = "$V{" + code + "}";
			logger.debug("Trying to find textfieldexpression value ending with: " + testStr);
			//get a list of all the textfieldExpression nodes
			NodeList nodes = doc.getElementsByTagName("textFieldExpression");
			for (int i = 0; i < nodes.getLength(); i++) {
				Node node = nodes.item(i);

				String textStr = node.getTextContent();
				if (textStr.endsWith(code + "}")) {
					Node textfield = node.getParentNode();
					textfield.getParentNode().removeChild(textfield);
				}
			}
			String result = convertDocToString(doc);
			//check that there were no errors in convertToString
			if (result != null) {
				return result;
			} else {
				return null;
			}

		}
		catch (ParserConfigurationException ex) {
			logger.error("There has been an error setting up the document builder factory: ", ex);
			return null;
		}
		catch (SAXException ex) {
			logger.error("There has been an error parsing the xml string passed as an argument: ", ex);
			return null;
		}
		catch (IOException ex) {
			logger.error("There has been an error parsing the xml string passed as an argument: ", ex);
			return null;
		}
		catch (Exception ex) {
			logger.error("There has been an error removing the field from the report: ", ex);
			return null;
		}
	}

	/*take in an xml doc object and convert it to a string
    Returns  - a string of xml if all is successful
     *           - null if there was a problem
	 */
	private String convertDocToString(Document doc) {
		Transformer transformer;
		String xmlString;
		//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd"
		try {
			transformer = TransformerFactory.newInstance().newTransformer();
			//HACCCKKKKKKKK to add doc type bak to top of document !!!!!!!!
			transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd");
			transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, "//JasperReports//DTD Report Design//EN");

			//initialize StreamResult with File object to save to file
			StreamResult result = new StreamResult(new StringWriter());
			DOMSource source = new DOMSource(doc);
			transformer.transform(source, result);
			xmlString = result.getWriter().toString();

		}
		catch (TransformerConfigurationException ex) {
			logger.debug("There has been an error configuring the transformer:", ex);
			return null;
		}
		catch (TransformerFactoryConfigurationError ex) {
			logger.debug("There has been an error configuring the transformer factory:", ex);
			return null;
		}
		catch (TransformerException ex) {
			logger.debug("There has been an error transforming the document: ", ex);
			return null;
		}
		catch (Exception ex) {
			logger.debug("There has been an error converting the Document object to a String: ", ex);
			return null;
		}
		return xmlString;
	}

	private String getRawCSVExport(int reportTypeID, Map<String, ReportParameterDefinitionDetails> paramDefs, Map<String, Object> parameters) throws SQLException {
		Connection conn = null;
		Statement statement = null;
		ResultSet result = null;
		try {
			String sql = getSQLForReport(reportTypeID, paramDefs, parameters);

			logger.debug("getRawCSVExport FINAL SQL [" + sql + "]");

			conn = DatabaseConnection.getReportsConnection();
			statement = conn.createStatement();
			logger.debug("exportRawCSV: Performing query and creating csv");
			//Perform the sql query
			statement.execute(sql);
			result = statement.getResultSet();

			StringWriter stringWriter = new StringWriter();
			CSVWriter csvWriter = new CSVWriter(stringWriter);

			//Build up headers
			ResultSetMetaData data = result.getMetaData();
			int numColumns = data.getColumnCount();
			String[] headerRow = new String[numColumns];
			for (int column = 1; column <= numColumns; column++) {//getColumnLabel
				headerRow[column - 1] = data.getColumnLabel(column);
			}
			//Write rowdata
			csvWriter.writeNext(headerRow);
			//Get first row
			boolean rowsLeft = result.next();
			while (rowsLeft) {
				String[] row = new String[numColumns];
				for (int column = 1; column <= numColumns; column++) {
					Object resultObject = result.getObject(column);
					if (resultObject != null) {
						String field;
						if (resultObject instanceof byte[]) {
							//Wonderfully fields that have been CONCAT()'d are VARBINARY.
							//If we have a byte array turn it into a string correctly.
							byte[] byteArray = (byte[]) resultObject;
							field = new String(byteArray, Charsets.UTF_8);
						} else {
							if (data.getColumnType(column) == Types.TIMESTAMP) {
								java.sql.Timestamp ts = (java.sql.Timestamp) resultObject;
								field = ReportingUtils.convertLongToTimeStampString(ts.getTime());
							} else if (data.getColumnType(column) == Types.TIME) {
								java.sql.Time time = (java.sql.Time) resultObject;
								field = ReportingUtils.convertLongToTimeString(time.getTime());
							} else {
								field = resultObject.toString();
							}
						}
						row[column - 1] = field;
					} else {
						int columnType = data.getColumnType(column);
						if (columnType == Types.BIGINT
								|| columnType == Types.DECIMAL
								|| columnType == Types.DOUBLE
								|| columnType == Types.FLOAT
								|| columnType == Types.INTEGER
								|| columnType == Types.NUMERIC
								|| columnType == Types.REAL
								|| columnType == Types.SMALLINT
								|| columnType == Types.TINYINT) {
							row[column - 1] = "0";
						} else {
							row[column - 1] = "";
						}

					}
				}
				csvWriter.writeNext(row);
				rowsLeft = result.next();
			}
			return stringWriter.toString();
		}
		finally {
			DatabaseConnection.quietlyCloseResultSet(result);
			DatabaseConnection.quietlyCloseStatement(statement);
			DatabaseConnection.quietlyCloseConnection(conn);
		}

	}

	private String getImportableHTMLExport(int reportDefID, Map<String, ReportParameterDefinitionDetails> paramDefs, Map<String, Object> parameters) throws SQLException {
		Connection conn = null;
		try {
			String sql = getSQLForReport(reportDefID, paramDefs, parameters);
			conn = DatabaseConnection.getReportsConnection();
			Statement statement = conn.createStatement();
			logger.debug("exportImportableHTML: Performing query and creating html");
			logger.debug("SQL is [\n" + sql + "\n]");
			//Perform the sql query
			statement.execute(sql);
			ResultSet result = statement.getResultSet();
			StringBuilder html = new StringBuilder();
			//Build up headers
			ResultSetMetaData data = result.getMetaData();
			int numColumns = data.getColumnCount();
			html.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">\n");
			html.append("\n<html>\n<head>\n<title>\n").append(getReportNameforReportTypeId(reportDefID)).append("</title>");
			html.append("\n<style type=\"text/css\">")
					.append("\ntable,td,th{border: 1px solid black;}")
					.append("\n</style>");
			html.append("\n</head>\n<body>");
			html.append("<table>");
			html.append("\n<tr>");
			for (int column = 1; column <= numColumns; column++) {
				html.append("\n<th>").append(data.getColumnLabel(column)).append("</th>");
			}
			html.append("\n</tr>");
			//Write rowdata
			//Get first row
			boolean rowsLeft = result.next();
			while (rowsLeft) {
				html.append("\n<tr>");
				for (int column = 1; column <= numColumns; column++) {
					Object resultObject = result.getObject(column);
					html.append("<td>");
					if (resultObject != null) {
						if (resultObject instanceof byte[]) {
							//Wonderfully fields that have been CONCAT()'d are VARBINARY.
							//If we have a byte array turn it into a string correctly.
							byte[] byteArray = (byte[]) resultObject;
							html.append(new String(byteArray));
						} else {
							html.append(resultObject);
						}
					}
					html.append("</td>");
				}
				html.append("\n</tr>");
				rowsLeft = result.next();
			}
			html.append("\n</table>\n</body>\n</html>\n");
			return html.toString();
		}
		finally {
			if (conn != null && !conn.isClosed()) {
				conn.close();
			}
		}
	}

	private String getSQLForReport(int reportTypeID, Map<String, ReportParameterDefinitionDetails> paramDefs, Map<String, Object> parameters) {
		logger.debug("getSQLForReport: retrieving and populating SQL Query");
		//Get the xml document of the report
		String xml = getXmlContentFromDB(reportTypeID);
		Document doc = DomHelper.transformXMLStringToDOMDocument(xml);
		//Get the sql from the report
		String sql = doc.getElementsByTagName("queryString").item(0).getTextContent();

		logger.debug("Retrieved sql [\n" + sql + "\n]");
		//Build up map of default values
		Map<String, String> defaultValues = new HashMap<String, String>();
		NodeList paramElements = doc.getElementsByTagName("parameter");
		for (int x = 0; x < paramElements.getLength(); x++) {
			Node paramNode = paramElements.item(x);
			String defvalue = "";
			NodeList nodes = paramNode.getChildNodes();
			for (int y = 0; y < nodes.getLength(); y++) {
				if (nodes.item(y).getNodeName().equals("defaultValueExpression")) {
					Pattern pattern = Pattern.compile("\\(\".*\"\\)");
					Matcher matcher = pattern.matcher(nodes.item(y).getTextContent());
					if (matcher.find()) {
						//Get the appropriate substring (Removing the enclosing brackets and quotes
						defvalue = nodes.item(y).getTextContent().substring(matcher.start() + 2, matcher.end() - 2);
						String name = ((Element) paramNode).getAttribute("name");
						logger.debug("param name = " + name);
						if (name.endsWith("DATE")) {
							logger.debug("getSQLForReport(): Found DATE param");//NOI18N
							String time = Integer.toString(_systemControllerLocal.getFringeThirtyHoursRule()) + ":" + "00'"; // default time
							// Check if some time is set
							if (defvalue.contains(":")) {
								if (time.length() < 5) {
									time = "0" + time;
								}
								int index = defvalue.indexOf(":");
								defvalue = defvalue.substring(0, index - 2) + time + defvalue.substring(index + 4);
							} else if (defvalue.equalsIgnoreCase(DATE_NOW)) {
								defvalue = "CONCAT(date(now()), ' ', '" + time + ")";
							} else if (!defvalue.equalsIgnoreCase(NOW)) {
								//date value with no time specified 
								defvalue = defvalue.trim() + " " + time;
							}
						}
					}
				}
			}
			logger.debug("Adding " + defvalue + " to " + ((Element) paramNode).getAttribute("name"));
			defaultValues.put(((Element) paramNode).getAttribute("name"), defvalue);
		}
		//Replace the params with appropriate values
		List<String> chosenGroups = newArrayList();
		List<String> allReportGroups = newArrayList();
		//add local server id to the sql
		String serverIdPlaceholder = "\\Q$P!{LOCALSERVERID}\\E";
		String serverParameterValue = (String) parameters.get("LOCALSERVERID");
		logger.debug("getSQLForReport: Replacing [" + serverIdPlaceholder + "] with [" + serverParameterValue + "]");
		sql = sql.replaceAll(serverIdPlaceholder, serverParameterValue);

		boolean summarise = false;
		for (String paramCode : paramDefs.keySet()) {
			ReportParameterDefinitionDetails parameterDef = paramDefs.get(paramCode);
			String placeholder = "\\Q$P!{" + paramCode + "}\\E";
			String parameterValue = (String) parameters.get(paramCode);
			if (parameterValue == null) {
				parameterValue = defaultValues.get(paramCode);
			}
			if (parameterDef != null) { // if param exists in DB not hard coded param like LOCALSERVERNETWORKID
				logger.debug(
						"GROUP parameter [" + parameterDef.getGroupOption() + "] CODE [" + parameterDef.getCode() + "] VALUE [" + parameterDef.getValue() + "]");
				if (parameterDef.getGroupOption() == 1) {
					String groupName = parameterDef.getCode().replace("GROUP", "").toLowerCase();
					if (!parameterDef.getValue().isEmpty()) {
						logger.debug("Group selected [" + groupName + "]");
						chosenGroups.add(groupName);
					}
					allReportGroups.add(groupName);
				}
				if (parameterDef.getCode().equals("SUMMARY") && !parameterDef.getValue().isEmpty()) {
					summarise = true;
				}
			}
			logger.debug("getSQLForReport: Replacing [" + placeholder + "] with [" + parameterValue + "]");
			sql = sql.replaceAll(placeholder, parameterValue);
		}

		logger.debug("getSQLForReport(): CHOSEN GROUPS [" + chosenGroups + "] ALL GROUPS [" + allReportGroups + "] summarise [" + summarise + "]");

		String regex = "GROUP BY (.+)(ORDER BY|HAVING |$)";
		Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
		Matcher matcher = pattern.matcher(sql);
		List<String> newGroupBys = newArrayList();
		if (matcher.find()) {
			Splitter splitter = Splitter.on(',').trimResults().omitEmptyStrings();
			Joiner joiner = Joiner.on(", ");
			String match = matcher.group(1);
			logger.debug("getSQLForReport(): found group by match [" + match + "]");
			Iterable<String> groupByGroups = splitter.split(match);
			for (String groupBy : groupByGroups) {
				logger.debug("getSQLForReport(): Group by group [" + groupBy + "]");
				if (chosenGroups.contains(groupBy) || (!allReportGroups.contains(groupBy) && !summarise)) {
					newGroupBys.add(groupBy);
				}
			}
			String newGroupByStatement = "";
			if (!newGroupBys.isEmpty()) {
				newGroupByStatement = "GROUP BY " + joiner.join(newGroupBys);
			} else {
				// TODO no group by so will need to remove HAVING if any in sql
				// leaving possibile ORDER BY
			}
			logger.debug("getSQLForReport(): new GROUP BY statement [" + newGroupByStatement + "]");
			StringBuffer sb = new StringBuffer();
			matcher.appendReplacement(sb, newGroupByStatement);
			matcher.appendTail(sb);
			sql = sb.toString();
		}
		return sql;
	}

}
