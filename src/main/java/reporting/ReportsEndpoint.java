package reporting;

import reporting.ExportEngine;
import catalog.dto.reporting.ReportExportFormat;
import catalog.dto.reporting.ReportParameterDefinitionDetails;
import catalog.dto.reporting.WebUserDetails;
import com.red61.via.api.exceptions.RestApiException;
import org.apache.log4j.Logger;
import reporting.engine.ReportingEngine;
import com.red61.via.api.auth.WebAuthenticatedUser;
import reporting.rest.dto.ReportDTO;
import reporting.rest.dto.ReportParamDTO;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import com.red61.via.api.auth.WebUserAuthRequired;

@Path("/reports")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@WebUserAuthRequired
public class ReportsEndpoint {

	@Inject
	@WebAuthenticatedUser
	private WebUserDetails webUserDetails;

	@Inject private ExportEngine exportEngine;

	private final Logger logger = Logger.getLogger(this.getClass().getName());

	/**
	 * ACTION: Run a saved report, with the saved parameters and export data type
	 *
	 * @param request
	 * @param savedReportId  Id of the saved report to run
	 * @param response
	 * @param dataType       Data type to export report as, defaults to "csv"
	 * @param paramOverrides Overridden parameter values, to replace what's in the currently saved report
	 * @return
	 */
	@POST
	@Path("/{savedReportId}/run")
	@Produces({MediaType.APPLICATION_JSON, "application/pdf", MediaType.TEXT_HTML, "application/rtf", MediaType.TEXT_PLAIN, "application/vnd.oasis.opendocument.text", "application/vnd.ms-excel", "text/csv", MediaType.TEXT_HTML})
	public Response runSavedReport(@Context HttpServletRequest request, @Context HttpServletResponse response,
			@PathParam("savedReportId") Integer savedReportId, @DefaultValue("csv") @QueryParam("dataType") ReportExportFormat dataType,
			List<ReportParamDTO> paramOverrides) {

		logger.debug("runSavedReport hit for report [" + savedReportId + "], data type [" + dataType + "]"); //NOI18N

		//TODO: find a way to get the info we need without loading all the params, since we aren't going to use them for running.
		ReportDTO reportDTO = reportingEngine.loadSavedReport(savedReportId, false, webUserDetails);

		Map<String, ReportParameterDefinitionDetails> paramDefs = reportingEngine.getParamDefsForReportDefId(savedReportId);

		// Replace overridden parameter defs
		paramDefs.putAll(reportingEngine.mapParamDTOs(reportDTO.getReportTypeId(), 0, paramOverrides, false));

		paramDefs = reportingEngine.populateEmptyParamDefsFromParameters(reportDTO.getReportTypeId(),
				savedReportId, paramDefs);

		if (paramDefs == null) {
			throw new RestApiException("Failed to load parameters", Response.Status.INTERNAL_SERVER_ERROR);
		}

		String defName = reportDTO.getName() == null ? "Not Available" : reportDTO.getName();
		String typeName = reportDTO.getType().getTitle() == null ? "Not Available" : reportDTO.getType().getTitle();

		String name = "Custom-" + typeName + "-" + defName;
		try {
			name = URLEncoder.encode(name, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.warn("Filename encoding failed: " + e.getMessage(), e); //NOI18N
			name = "Custom-Report";
		}

		OutputStream outputStream = exportEngine.exportReportToOutputStream(reportDTO.getReportTypeId(), paramDefs, dataType, name, webUserDetails);

		return Response.ok(outputStream).header("Content-Type", dataType.getMediaType()).header("Content-Disposition",
				"attachment; filename=\"" + name + "." + dataType.getFileExtension() + "\"").build();
	}
}
